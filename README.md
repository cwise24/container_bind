# Containerized DNS

Container is built on Alpine
 * named.conf
 * a.zone (forward zone)
 * ptr.zone (reverse zone)

# Folder Layout

I had created all the content under the *bind* directory and set the folder permissions to `777`
```
bind
|---- a.zone
|---- Dockerfile
|---- named.conf 
|---- ptr.zone
```

# Build Container Image
Create container image (*dnsi*): <br />
`docker build -t dnsi Dockerfile` <br />

# Build customer network

```
docker network create --driver=bridge --gateway=172.10.1.1 --subnet=172.10.1.0/24 docnet
```

How do I see my containers IP Address?
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id
```

# Start Container

## Docker-compose
```
docker-compose up -d
```
## Manually
```
docker run --net <docnet> --ip 172.1.20.53 \
-p 53:53/udp -p 53:53/tcp \
-v ~/bind/:/var/bind/ -v ~/bind/named.conf:/etc/bind/named.conf \
--name dns -dit dnsi
```
# POST INSTALL

## File Permissions
Open a shell with the newly created container:
* `docker exec -it dns /bin/sh` <br />
* `chown root:named /etc/bind/rndc.key` <br />
* `chmod 640 /etc/bind/rndc.key`

This allows you to reload named after updates: <br />
`docker exec -it dns rndc reload`